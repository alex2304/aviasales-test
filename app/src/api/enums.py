import enum


class ChargeTypes(enum.Enum):
    ADULT = 1
    CHILD = 2
    INFANT = 3

    @classmethod
    def value_of(cls, name):
        for e in cls:
            if e.name == name:
                return e.value
        raise ValueError(name)

    @classmethod
    def choices(cls):
        return [(e.value, e.name)
                for e in cls]

