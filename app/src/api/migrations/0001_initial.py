# Generated by Django 2.2.4 on 2019-08-21 08:01

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Airport',
            fields=[
                ('code', models.CharField(max_length=5, primary_key=True, serialize=False, verbose_name='Код IATA')),
            ],
            options={
                'verbose_name': 'Аэропорт',
                'verbose_name_plural': 'Аэропорты',
                'db_table': 'airports',
            },
        ),
        migrations.CreateModel(
            name='Carrier',
            fields=[
                ('code', models.CharField(max_length=5, primary_key=True, serialize=False, verbose_name='Код IATA')),
                ('name', models.CharField(max_length=100, verbose_name='Название')),
            ],
            options={
                'verbose_name': 'Перевозчик',
                'verbose_name_plural': 'Перевозчики',
                'db_table': 'carriers',
            },
        ),
        migrations.CreateModel(
            name='Flight',
            fields=[
                ('number', models.PositiveIntegerField(primary_key=True, serialize=False, verbose_name='Номер рейса')),
                ('flight_class', models.CharField(max_length=5, verbose_name='Класс')),
                ('ticket_type', models.CharField(max_length=5, verbose_name='Тип билета')),
                ('fare_basis', models.CharField(max_length=200, verbose_name='Fare basis')),
                ('has_stops', models.BooleanField(verbose_name='С остановками')),
                ('carrier', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='api.Carrier', verbose_name='Перевозчик')),
                ('destination', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='destinations', to='api.Airport', verbose_name='Пункт назначения')),
                ('source', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='sources', to='api.Airport', verbose_name='Пункт отправления')),
            ],
            options={
                'verbose_name': 'Перелет',
                'verbose_name_plural': 'Перелеты',
                'db_table': 'flight',
            },
        ),
        migrations.CreateModel(
            name='Itinerary',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('total_cost', models.FloatField(verbose_name='Общая стоимость')),
                ('onward_duration_s', models.FloatField(verbose_name='Продолжительность туда, c')),
                ('return_duration_s', models.FloatField(verbose_name='Продолжительность обратно, с')),
                ('total_duration_s', models.FloatField(verbose_name='Общая продолжительность, с')),
                ('has_return', models.BooleanField(verbose_name='Есть обратный маршрут')),
                ('has_child', models.BooleanField(verbose_name='Летит ребенок')),
                ('has_infant', models.BooleanField(verbose_name='Летит младенец')),
                ('destination', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='itineraries_to', to='api.Airport')),
                ('source', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='itineraries_from', to='api.Airport')),
            ],
            options={
                'verbose_name': 'Маршрут',
                'verbose_name_plural': 'Маршруты',
                'db_table': 'itinerary',
            },
        ),
        migrations.CreateModel(
            name='ItineraryFlight',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('is_return', models.BooleanField(verbose_name='Это обратный маршрут?')),
                ('departure_ts', models.DateTimeField(verbose_name='Время вылета')),
                ('arrival_ts', models.DateTimeField(verbose_name='Время прибытия')),
                ('flight', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='itineraries', to='api.Flight', verbose_name='Перелет')),
                ('itinerary', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='flights', to='api.Itinerary', verbose_name='Маршрут')),
            ],
            options={
                'verbose_name': 'Перелет маршрута',
                'verbose_name_plural': 'Перелеты маршрута',
                'db_table': 'itinerary_flight_m2m',
            },
        ),
        migrations.CreateModel(
            name='Charges',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('currency', models.CharField(max_length=5, verbose_name='Валюта')),
                ('charge_type', models.PositiveSmallIntegerField(choices=[(1, 'ADULT'), (2, 'CHILD'), (3, 'INFANT')], verbose_name='Тип цены')),
                ('base_fare', models.FloatField(verbose_name='Тариф')),
                ('airline_taxes', models.FloatField(default=0, verbose_name='Налоги')),
                ('total_amount', models.FloatField(verbose_name='Общая стоимость')),
                ('itinerary', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='charges', to='api.Itinerary', verbose_name='маршрут')),
            ],
            options={
                'verbose_name': 'Стоимость',
                'verbose_name_plural': 'Стоимости',
                'db_table': 'charges',
            },
        ),
    ]
