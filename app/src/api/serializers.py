from drf_yasg.utils import swagger_serializer_method
from rest_framework import serializers

from . import models


class AirportSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Airport
        fields = '__all__'


class CarrierSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Carrier
        fields = '__all__'


class ChargesSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Charges
        fields = ('currency', 'charge_type', 'base_fare', 'airline_taxes', 'total_amount')


class FlightSerializer(serializers.ModelSerializer):
    carrier = CarrierSerializer()
    source = AirportSerializer()
    destination = AirportSerializer()

    class Meta:
        model = models.Flight
        fields = ('carrier',
                  'source',
                  'destination',
                  'number',
                  'flight_class',
                  'ticket_type',
                  'fare_basis',
                  'has_stops')


class ItineraryFlightSerializer(serializers.ModelSerializer):
    flight = FlightSerializer()

    class Meta:
        model = models.ItineraryFlight
        fields = ('departure_ts', 'arrival_ts', 'flight')


class ItinerarySerializer(serializers.ModelSerializer):
    charges = ChargesSerializer(many=True)
    onward_flights = serializers.SerializerMethodField()
    return_flights = serializers.SerializerMethodField()

    class Meta:
        model = models.Itinerary
        fields = ('total_cost',
                  'total_duration_s',
                  'onward_duration_s',
                  'return_duration_s',
                  'charges',
                  'onward_flights',
                  'return_flights')

    @swagger_serializer_method(ItineraryFlightSerializer)
    def get_onward_flights(self, itinerary):
        qs = itinerary.flights.filter(is_return=False).order_by('departure_ts')
        return ItineraryFlightSerializer(instance=qs.all(), many=True).data

    @swagger_serializer_method(ItineraryFlightSerializer)
    def get_return_flights(self, itinerary):
        qs = itinerary.flights.filter(is_return=True).order_by('departure_ts')
        return ItineraryFlightSerializer(instance=qs.all(), many=True).data
