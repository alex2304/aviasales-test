from django.db import models

from . import enums


class Itinerary(models.Model):
    total_cost = models.FloatField('Общая стоимость')
    onward_duration_s = models.FloatField('Продолжительность туда, c')
    return_duration_s = models.FloatField('Продолжительность обратно, с')
    total_duration_s = models.FloatField('Общая продолжительность, с')

    # Fields are stored for filtering optimisation
    source = models.ForeignKey('Airport', on_delete=models.CASCADE, related_name='itineraries_from')
    destination = models.ForeignKey('Airport', on_delete=models.CASCADE, related_name='itineraries_to')
    has_return = models.BooleanField('Есть обратный маршрут')
    has_child = models.BooleanField('Летит ребенок')
    has_infant = models.BooleanField('Летит младенец')

    class Meta:
        db_table = 'itinerary'
        verbose_name = 'Маршрут'
        verbose_name_plural = 'Маршруты'


class Charges(models.Model):
    currency = models.CharField('Валюта', max_length=5)
    charge_type = models.PositiveSmallIntegerField('Тип цены', choices=enums.ChargeTypes.choices())
    base_fare = models.FloatField('Тариф')
    airline_taxes = models.FloatField('Налоги', default=0)
    total_amount = models.FloatField('Общая стоимость')

    itinerary = models.ForeignKey('Itinerary', on_delete=models.CASCADE, related_name='charges', verbose_name='маршрут')

    class Meta:
        db_table = 'charges'
        verbose_name = 'Стоимость'
        verbose_name_plural = 'Стоимости'


class Flight(models.Model):
    id = None
    number = models.PositiveIntegerField('Номер рейса', primary_key=True)

    carrier = models.ForeignKey('Carrier', on_delete=models.CASCADE, verbose_name='Перевозчик', to_field='code')
    source = models.ForeignKey('Airport', on_delete=models.CASCADE, verbose_name='Пункт отправления',
                               related_name='sources', to_field='code')
    destination = models.ForeignKey('Airport', on_delete=models.CASCADE, verbose_name='Пункт назначения',
                                    related_name='destinations', to_field='code')
    flight_class = models.CharField('Класс', max_length=5)
    ticket_type = models.CharField('Тип билета', max_length=5)
    fare_basis = models.CharField('Fare basis', max_length=200)
    has_stops = models.BooleanField('С остановками')

    class Meta:
        db_table = 'flight'
        verbose_name = 'Перелет'
        verbose_name_plural = 'Перелеты'


class ItineraryFlight(models.Model):
    itinerary = models.ForeignKey('Itinerary', on_delete=models.CASCADE, related_name='flights', verbose_name='Маршрут')
    flight = models.ForeignKey('Flight', on_delete=models.CASCADE, related_name='itineraries', verbose_name='Перелет')
    is_return = models.BooleanField('Это обратный маршрут?')
    departure_ts = models.DateTimeField('Время вылета')
    arrival_ts = models.DateTimeField('Время прибытия')

    class Meta:
        db_table = 'itinerary_flight_m2m'
        verbose_name = 'Перелет маршрута'
        verbose_name_plural = 'Перелеты маршрута'


class Carrier(models.Model):
    id = None
    code = models.CharField('Код IATA', max_length=5, primary_key=True)
    name = models.CharField('Название', max_length=100)

    class Meta:
        db_table = 'carriers'
        verbose_name = 'Перевозчик'
        verbose_name_plural = 'Перевозчики'


class Airport(models.Model):
    id = None
    code = models.CharField('Код IATA', max_length=5, primary_key=True)

    class Meta:
        db_table = 'airports'
        verbose_name = 'Аэропорт'
        verbose_name_plural = 'Аэропорты'
