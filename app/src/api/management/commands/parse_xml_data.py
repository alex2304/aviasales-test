from django.core.management import BaseCommand

from external.parser import XMLDataParser


class Command(BaseCommand):
    help = 'Parse itineraries data from xml files and upload into the db'

    def handle(self, *args, **options):
        parser = XMLDataParser()
        parser.parse_from_file('external/RS_Via-3.xml')
        parser.parse_from_file('external/RS_ViaOW.xml')

        parser.load_into_db()

        self.stdout.write(self.style.SUCCESS('\nData has been parsed and uploaded into the db'))
