from rest_framework import viewsets, mixins

from . import models, serializers, filters


class ItinerariesViewSet(viewsets.GenericViewSet,
                         mixins.ListModelMixin):
    queryset = models.Itinerary.objects.all()
    serializer_class = serializers.ItinerarySerializer
    filter_class = filters.ItinerariesFilter

    def list(self, request, *args, **kwargs):
        """Query for itineraries list

        Charge types: 1 - ADULT, 2 - CHILD, 3 - INFANT
        """
        return super(ItinerariesViewSet, self).list(request, *args, **kwargs)


class AirportsViewSet(viewsets.GenericViewSet,
                      mixins.ListModelMixin):
    queryset = models.Airport.objects.order_by('code').all()
    serializer_class = serializers.AirportSerializer


class FlightsViewSet(viewsets.GenericViewSet,
                     mixins.ListModelMixin):
    queryset = models.Flight.objects.order_by('number').all()
    serializer_class = serializers.FlightSerializer


class CarriersViewSet(viewsets.GenericViewSet,
                      mixins.ListModelMixin):
    queryset = models.Carrier.objects.order_by('name').all()
    serializer_class = serializers.CarrierSerializer
