from django_filters import rest_framework as filters

from . import models


class ItinerariesFilter(filters.FilterSet):
    src = filters.CharFilter(field_name='source', label='Source, for ex. DXB')
    dest = filters.CharFilter(field_name='destination', label='Destination, for ex. BKK')
    has_return = filters.BooleanFilter(field_name='has_return', label='Has return flights, true/false')
    child = filters.BooleanFilter(field_name='has_child', label='For child, true/false')
    infant = filters.BooleanFilter(field_name='has_infant', label='For infant, true/false')

    @property
    def qs(self):
        queryset = super(ItinerariesFilter, self).qs

        sorting = self.request.query_params.get('sorting')

        if sorting == 'cheapest':
            queryset = queryset.order_by('total_cost', 'total_duration_s')
        elif sorting == 'fastest':
            queryset = queryset.order_by('total_duration_s', '-total_cost')
        elif sorting == 'optimal':
            queryset = queryset.order_by('total_duration_s', 'total_cost')

        return queryset

    class Meta:
        model = models.Itinerary
        fields = ('src', 'dest', 'has_return', 'child', 'infant')
