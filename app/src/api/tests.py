import http
from urllib import parse

from rest_framework.test import APITestCase

from core import helpers
from external.parser import XMLDataParser


class BaseTestCase(APITestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        parser = XMLDataParser()
        parser.parse_from_file('external/RS_Via-3.xml')
        parser.parse_from_file('external/RS_ViaOW.xml')

        parser.load_into_db()


class TestListMixin:
    _paginated_response_fields = {'count', 'next', 'previous', 'results'}
    _result_fields = None

    def test_list_200(self):
        if self._result_fields:
            url = self.reverse_view_url('list')
            response = self.client.get(url)
            self.assertEqual(response.status_code, http.HTTPStatus.OK, response.data)

            self.assertSetEqual(set(response.data), self._paginated_response_fields)
            self.assertSetEqual(set(response.data['results'][0]), self._result_fields)


class AirportsTestCase(BaseTestCase,
                       TestListMixin,
                       helpers.ViewSetTestMixin):
    _basename = 'airports'
    _result_fields = {'code'}


class CarriersTestCase(BaseTestCase,
                       TestListMixin,
                       helpers.ViewSetTestMixin):
    _basename = 'carriers'
    _result_fields = {'code', 'name'}


class FlightsTestCase(BaseTestCase,
                      TestListMixin,
                      helpers.ViewSetTestMixin):
    _basename = 'flights'
    _result_fields = {
        'carrier',
        'source',
        'destination',
        'number',
        'flight_class',
        'ticket_type',
        'fare_basis',
        'has_stops',
    }


class ItinerariesTestCase(BaseTestCase,
                          TestListMixin,
                          helpers.ViewSetTestMixin):
    _basename = 'itineraries'
    _result_fields = {
        'total_cost',
        'total_duration_s',
        'onward_duration_s',
        'return_duration_s',
        'charges',
        'onward_flights',
        'return_flights',
    }

    def test_list_filters(self):
        url = self.reverse_view_url('list')

        filters = (
            dict(src='DXB', dest='BKK', has_return='true', child='false', infant='false'),
            dict(src='DXB', dest='BKK', has_return='false', child='true', infant='true'),
            dict(src='DXB', dest='BKK', sorting='fastest'),
            dict(src='DXB', dest='BKK', sorting='cheapest'),
            dict(src='DXB', dest='BKK', sorting='optimal'),
        )
        for params in filters:
            filters_url = '%s?%s' % (url, parse.urlencode(params))
            response = self.client.get(filters_url)

            self.assertEqual(response.status_code, http.HTTPStatus.OK, (params, response.data))
            self.assertSetEqual(set(response.data['results'][0]), self._result_fields, params)
