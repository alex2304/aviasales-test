import os
from collections import defaultdict
from datetime import datetime
from os import path
from xml.etree import ElementTree

import sorcery
import stringcase
from django.db import transaction
from tqdm import tqdm

from api.enums import ChargeTypes
from external import utils


class XMLDataParser:
    """Class for parsing itineraries from xml files.

    Can parse itineraries and related entities from several files.

    Parsed entities can be uploaded into the database using designed models.

    Usage:
        >> parser = XMLDataParser()
        >> parser.parse_from_file('file_1.xml')
        >> parser.parse_from_file('file_2.xml')
        >> parser.load_into_db()
    """
    _file_dt_format = '%Y-%m-%dT%H%M'

    def __init__(self):
        self._carriers = set()
        self._airports = set()
        self._flights = set()
        self._itineraries = []

    def parse_from_file(self, xml_file_name):
        tree = ElementTree.parse(xml_file_name)
        root = tree.getroot().find('PricedItineraries')

        for itinerary in root.findall('Flights'):
            self._parse_itinerary(itinerary)

    def _parse_itinerary(self, itinerary):
        # parse onward flights of itinerary
        onward_flights = itinerary.find('OnwardPricedItinerary')
        onward_flights = [self._parse_flight(flight)
                          for flight in onward_flights.find('Flights').findall('Flight')]

        # parse return flights of itinerary, if exist
        return_flights_tag = itinerary.find('ReturnPricedItinerary')
        return_flights = []
        if return_flights_tag:
            return_flights = [self._parse_flight(flight)
                              for flight in return_flights_tag.find('Flights').findall('Flight')]

        # parse charges from itinerary pricing
        pricing = itinerary.find('Pricing')
        charges = self._parse_pricing(pricing)

        # calculate total itinerary cost
        total_cost = sum(float(charge['total_amount'])
                         for charge in charges)

        # calculate itinerary duration
        onward_duration = (onward_flights[-1]['arrival_ts'] - onward_flights[0]['departure_ts']).total_seconds()
        return_duration = 0
        if return_flights:
            return_duration = (return_flights[-1]['arrival_ts'] - return_flights[0]['departure_ts']).total_seconds()

        # itinerary data has format adopted for db model
        itinerary_data = dict(total_cost=total_cost,
                              onward_duration_s=onward_duration,
                              return_duration_s=return_duration,
                              total_duration_s=onward_duration + return_duration,
                              charges=charges,
                              onward_flights=[dict(flight_id=flight['number'],
                                                   arrival_ts=flight['arrival_ts'],
                                                   departure_ts=flight['departure_ts']) for flight in onward_flights],
                              return_flights=[dict(flight_id=flight['number'],
                                                   arrival_ts=flight['arrival_ts'],
                                                   departure_ts=flight['departure_ts']) for flight in return_flights])
        # parse auxiliary values
        has_child, has_infant = False, False
        for charge in charges:
            if charge['charge_type'] == ChargeTypes.CHILD.value:
                has_child = True
            elif charge['charge_type'] == ChargeTypes.INFANT.value:
                has_infant = True
        itinerary_data.update(has_return=bool(return_flights),
                              has_child=has_child,
                              has_infant=has_infant,
                              source=onward_flights[0]['source'],
                              destination=onward_flights[-1]['destination'])

        self._itineraries.append(itinerary_data)

    def _parse_flight(self, flight) -> dict:
        # parse the carrier of the flight
        carrier = flight.find('Carrier')
        carrier_id = carrier.get('id')
        carrier_name = carrier.text
        self._carriers.add(utils.Hashabledict(code=carrier_id,
                                              name=carrier_name))

        # parse the source and destination of the flight
        source, destination = (flight.find('Source').text,
                               flight.find('Destination').text)
        self._airports.add(utils.Hashabledict(code=source))
        self._airports.add(utils.Hashabledict(code=destination))

        # parse and store the flight
        flight = utils.Hashabledict(
            carrier=carrier_id,

            source=source,
            destination=destination,

            number=int(flight.find('FlightNumber').text),
            departure_ts=self._parse_dt(flight.find('DepartureTimeStamp').text),
            arrival_ts=self._parse_dt(flight.find('ArrivalTimeStamp').text),
            flight_class=flight.find('Class').text,
            has_stops=int(flight.find('NumberOfStops').text) > 0,
            fare_basis=flight.find('FareBasis').text.replace('\n', '').strip(),
            ticket_type=flight.find('TicketType').text
        )
        self._flights.add(flight)

        return flight

    def _parse_dt(self, value: str) -> datetime:
        return datetime.strptime(value, self._file_dt_format)

    def _parse_pricing(self, pricing) -> list:
        currency = pricing.get('currency')

        # parse charges from pricing
        charges = defaultdict(dict)
        for tag in pricing.findall('ServiceCharges'):
            charge_type = self._get_charge_type(tag.get('type'))
            obj_attr = stringcase.snakecase(tag.get('ChargeType'))  # BaseFare -> base_fare, etc.
            value = tag.text
            charges[charge_type][obj_attr] = value

        return [dict(currency=currency,
                     charge_type=charge_type,
                     **attrs)
                for charge_type, attrs in charges.items()]

    @staticmethod
    def _get_charge_type(service_charge_type):
        if service_charge_type == 'SingleAdult':
            return ChargeTypes.ADULT.value
        elif service_charge_type == 'SingleChild':
            return ChargeTypes.CHILD.value
        elif service_charge_type == 'SingleInfant':
            return ChargeTypes.INFANT.value
        else:
            raise NotImplementedError(service_charge_type)

    def load_into_db(self):
        import django
        os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'core.settings')
        django.setup()

        from api import models

        with transaction.atomic():
            models.Itinerary.objects.all().delete()

            # create carriers
            for carrier in tqdm(self._carriers, 'Carriers'):
                models.Carrier.objects.update_or_create(**carrier)

            # create airports
            for airport in tqdm(self._airports, 'Airports'):
                models.Airport.objects.update_or_create(**airport)

            # lookup flights by number, and update
            for flight in tqdm(self._flights, 'Flights'):
                # pass object_id instead of object
                carrier_id = flight.pop('carrier')
                source_id = flight.pop('source')
                destination_id = flight.pop('destination')
                flight.update(**sorcery.dict_of(carrier_id, source_id, destination_id))

                flight_number, *_ = (flight.pop('number'),
                                     flight.pop('departure_ts'),
                                     flight.pop('arrival_ts'))
                models.Flight.objects.update_or_create(defaults=flight,
                                                       number=flight_number)

            # create itineraries and dependent entities
            for itinerary in tqdm(self._itineraries, 'Itineraries'):
                onward_flights = itinerary.pop('onward_flights')
                return_flights = itinerary.pop('return_flights')
                charges = itinerary.pop('charges')

                source_id = itinerary.pop('source')
                destination_id = itinerary.pop('destination')
                itinerary.update(**sorcery.dict_of(source_id, destination_id))

                itinerary_obj = models.Itinerary.objects.create(**itinerary)

                # create charges
                for c in charges:
                    models.Charges.objects.update_or_create(itinerary=itinerary_obj, **c)

                # create m2m flight <-> itinerary
                for flight in onward_flights:
                    models.ItineraryFlight.objects.create(itinerary=itinerary_obj,
                                                          is_return=False,
                                                          **flight)
                for flight in return_flights:
                    models.ItineraryFlight.objects.create(itinerary=itinerary_obj,
                                                          is_return=True,
                                                          **flight)


def test_parser():
    dir_path = path.dirname(path.abspath(__file__))

    parser = XMLDataParser()
    parser.parse_from_file('%s/RS_Via-3.xml' % dir_path)
    parser.parse_from_file('%s/RS_ViaOW.xml' % dir_path)

    parser.load_into_db()


if __name__ == '__main__':
    test_parser()
