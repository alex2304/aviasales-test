from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include
from rest_framework import routers, permissions
from drf_yasg import openapi
from drf_yasg.views import get_schema_view

import api.views as api

router = routers.DefaultRouter(trailing_slash=True)
router.register('airports', api.AirportsViewSet, base_name='airports')
router.register('carriers', api.CarriersViewSet, base_name='carriers')
router.register('flights', api.FlightsViewSet, base_name='flights')
router.register('itineraries', api.ItinerariesViewSet, base_name='itineraries')

docs_schema_view = get_schema_view(
    openapi.Info(
        title='Aviasales test API',
        default_version='v1',
        description='N.B. documentation is auto-generated, so some sections may be wrong. '
                    'Ask developers if you have any issues.',
        terms_of_service='',
        contact=openapi.Contact(name='Aleksey', email='alex2304el@gmail.com'),
        license=openapi.License(name='BSD License'),
    ),
    validators=['flex', 'ssv'],
    public=True,
    permission_classes=(permissions.AllowAny,),
)

docs_urls = [
    path('swagger/', docs_schema_view.with_ui('swagger'), name='schema-swagger-ui'),
    path('redoc/', docs_schema_view.with_ui('redoc'), name='schema-redoc'),
]

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include(router.urls)),
    path('docs/', include(docs_urls))
]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
