from rest_framework.reverse import reverse


class ViewSetTestMixin:
    """Allows to reverse action urls of a ViewSet.

    How to use:
        1) Inherit from the class
        2) Override the `_basename` attribute
        3) Use the `self.reverse_view_url()` function

    Example:
        class TestCase(rest_framework.test.APITestCase):
            _basename = ''  # base name of a ViewSet (specified in a Router)

            def test_case(self):
                # get urls for actions of the ViewSet

                url = self.reverse_view_url('create')
                url = self.reverse_view_url('destroy', pk=1)
                url = self.reverse_view_url('list')
                url = self.reverse_view_url('retrieve', pk=1)
                url = self.reverse_view_url('partial_update', pk=1)
                url = self.reverse_view_url('update', pk=1)

                url = self.reverse_view_url('custom_action_name')
                url = self.reverse_view_url('custom_action_name', action_param_1="value")
    """
    _basename = None

    def reverse_view_url(self, action=None, request=None, **kwargs):
        """Reverses view url for `self._basename` and given action.

        For list of default actions, see http://www.django-rest-framework.org/api-guide/routers/#simplerouter

        Raises `django.urls.NoReverseMatch`, if it's unable to reverse the url.
        """
        assert self._basename is not None, 'Improperly configured: override "_basename"'

        if action is not None:
            view_name = self._get_view_name(action)
        else:
            view_name = self._basename

        return reverse(view_name, kwargs=kwargs, request=request)

    def _get_view_name(self, action_name):
        if action_name in ('list', 'create'):
            action = 'list'
        elif action_name in ('retrieve', 'update', 'partial_update', 'destroy'):
            action = 'detail'
        else:
            action = action_name

        return '%s-%s' % (self._basename, action)
